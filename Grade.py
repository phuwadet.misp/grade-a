import sqlite3
from tkinter import messagebox
from tkinter import *

def mainwindow() :
    root = Tk()
    w = 1000
    h = 800
    x = root.winfo_screenwidth()/2 - w/2
    y = root.winfo_screenheight()/2 - h/2
    root.geometry("%dx%d+%d+%d"%(w,h,x,y))
    root.config(bg='#28527a')
    #root.config(bg='#4a3933')
    root.title("Student By Phuwadet ")
    root.option_add('*font',"Garamond 24 bold")
    root.rowconfigure((0,1,2,3),weight=1)
    root.columnconfigure((0,1,2,3),weight=1)
    return root

def loginlayout(root) : 
    global userentry
    global pwdentry
    global loginframe
    
    loginframe = Frame(root,bg='#709fb0')
    loginframe.rowconfigure((0,1,2,3),weight=1)
    loginframe.columnconfigure((0,1),weight=1)
    
    Label(loginframe,text="Account Login",font="Garamond 26 bold",image=img1,compound=TOP,bg='#709fb0',fg='#e4fbff').grid(row=0,columnspan=2)
    Label(loginframe,text="User name : ",bg='#709fb0',fg='#e4fbff',padx=20).grid(row=1,column=0,sticky='e')
    userentry = Entry(loginframe,bg='#e4fbff',width=20,textvariable=userinfo)
    userentry.grid(row=1,column=1,sticky='w',padx=20)
    pwdentry = Entry(loginframe,bg='#e4fbff',width=20,show='*',textvariable=pwdinfo)
    pwdentry.grid(row=2,column=1,sticky='w',padx=20)
    Label(loginframe,text="Password  : ",bg='#709fb0',fg='#e4fbff',padx=20).grid(row=2,column=0,sticky='e')
    Button(loginframe,text="Login",width=10,command=lambda:loginclick(userinfo.get(),pwdinfo.get()),bg='lightgreen').grid(row=3,column=1,pady=20,ipady=15,sticky='e',padx=20)
    Button(loginframe,text="Register",width=10,command=regiswindow,bg='lightblue').grid(row=3,column=1,pady=20,ipady=15,sticky='w',padx=(0,200))
    Button(loginframe,text="Exit",width=10,command=quit).grid(row=3,column=0,pady=20,ipady=15,padx=20)
    loginframe.grid(row=1,column=1,columnspan=2,rowspan=2,sticky='news')

def createconnection() :
    global conn,curr
    conn = sqlite3.connect('data.db')
    curr = conn.cursor()
def loginclick(userinfo,pwdinfo):
    
    curr.execute(f'SELECT * FROM student where  USERNAME="{userinfo}" and PASSWORD = "{pwdinfo}" ')
    studen = curr.fetchone()
    
       
    if userinfo == "" :
        messagebox.showwarning('Admin:','Enter username')
        userentry.focus_force()
    else :
        curr.execute(f'SELECT * FROM student where  USERNAME="{userinfo}"')
        result = curr.fetchall()
        if result :            
           if pwdinfo == "" :
               messagebox.showwarning('Admin:','Enter Password first')
               pwdentry.focus_force()
           else :
                curr.execute(f'SELECT * FROM student where USERNAME= "{userinfo}" and PASSWORD ="{pwdinfo}"')
                resulr = curr.fetchall()
                if resulr:
                   def llow()  :
                        userentry.delete(0,END)
                        pwdentry.delete(0,END)
                        frame1.destroy()
                        loginlayout(root)
                           
                   messagebox.showinfo("Admin:",'Login Successfully')
                   frame1 = Frame(root,bg='pink')
                   frame1.place(x=0,y=0,width=1000,height=800)
                
                   Label(frame1,image=img1,bg='pink').place(x = 400, y= 10)
                   Button(frame1,text="LOG OUT",width=15,command=llow).place(x = 350, y= 500)
                   

                   sb = Label(frame1,text ="En002",font = " 10")
                   sb2 = Label(frame1,text ="cs436",font = " 10")
                   sb3 = Label(frame1,text ="cs318",font = " 10")
                   sb4 = Label(frame1,text ="cs350",font = " 10")
                   sb5 = Label(frame1,text ="it494",font = " 10")
                   
                   total = Label(frame1,text ="Total",font = " 10")
                   avg = Label(frame1,text ="Avrage",font = " 10")
                   grade = Label(frame1,text ="Grade",font = " 10")
                   
                   eng=StringVar()
                   cs1 =StringVar()
                   cs2 =StringVar()
                   cs3 =StringVar()
                   it =StringVar()
                   sb.place(x=50,y=20)
                   sb2.place(x=50,y=70)
                   sb3.place(x=50,y=120)
                   sb4.place(x=50,y=170)
                   sb5.place(x=50,y=210)
                   total.place(x=50,y=250)
                   avg.place(x=50,y=300)
                   grade.place(x=50,y=350)
                   en = Entry(frame1,textvariable = eng,font = " 10",width=15)
                   en2 = Entry(frame1,textvariable = cs1,font = " 10",width=15)
                   en3 = Entry(frame1,textvariable = cs2,font = " 10",width=15)
                   en4 = Entry(frame1,textvariable = cs3,font = " 10",width=15) 
                   en5 = Entry(frame1,textvariable = it,font = " 10",width=15)

                   en.place(x = 250,y=20)
                   en2.place(x = 250,y=70)
                   en3.place(x = 250,y=120)
                   en4.place(x = 250,y=170)
                   en5.place(x = 250,y=210)
                   def Calculation():
                      en002 = int(en.get())
                      cs436 = int(en2.get())
                      cs318 = int(en3.get())
                      cs350 = int(en4.get())
                      it494 = int(en5.get())
                      total = (en002+cs436+cs318+cs350+it494)
                      Label(text =f"{total}",font=" 10").place(x=250,y=250)
                      average=int(total/5)
                      Label(text =f"{average}",font=" 10").place(x=250,y=300)

                      if(average>50):
                            grade = 'PASS'
                      else :
                            grade = "Fail"
                      Label(text=f"{grade}",font=" 10").place(x=250,y=350)
                   Button(frame1,text='Calculate',font = ' 10',bg='gray',bd=10,command=Calculation).place(x=50,y=400)


                else :
                    messagebox.showerror('Admin:','Username or Password is invalid')
                    pwdentry.focus_force()

        else : 
            messagebox.showerror('Admin : ','Username or Password is invalid.')
            userentry.focus_force()

def regiswindow() :
    global stid,firstname,lastname,newuser,newpwd,cfpwd,regisframe

    loginframe.destroy()
    
    root.title("Welcome to User Registration : ")
    root.config(bg='lightblue')
    regisframe = Frame(root,bg='#8ac4d0')
    regisframe.rowconfigure((0,1,2,3,4,5,6,7,8,9,10,11),weight=1)
    regisframe.columnconfigure((0,1),weight=1)
    Label(regisframe,text="Registration From",font="Garamond 26 bold",fg='#e4fbff',image=img1,compound=LEFT,bg='#1687a7').grid(row=0,column=0,columnspan=2,sticky='news',pady=10)
    
    Label(regisframe,text='Student ID : ',bg='#8ac4d0',fg='#f6f5f5').grid(row=1,column=0,sticky='e',padx=10)
    stid = Entry(regisframe,width=20,bg='#d3e0ea',textvariable=ids)
    stid.grid(row=1,column=1,sticky='w',padx=10)
    
    Label(regisframe,text='First name: ',bg='#8ac4d0',fg='#f6f5f5').grid(row=2,column=0,sticky='e',padx=10)
    firstname = Entry(regisframe,width=20,bg='#d3e0ea',textvariable=fname)
    firstname.grid(row=2,column=1,sticky='w',padx=10)
    
    Label(regisframe,text="Last name : ",bg='#8ac4d0',fg='#f6f5f5').grid(row=3,column=0,sticky='e',padx=10)
    lastname = Entry(regisframe,width=20,bg='#d3e0ea',textvariable=lname)
    lastname.grid(row=3,column=1,sticky='w',padx=10)
        
    Label(regisframe,text="Username  : ",bg='#8ac4d0',fg='#f6f5f5').grid(row=8,column=0,sticky='e',padx=10) 
    newuser = Entry(regisframe,width=20,bg='#d3e0ea',textvariable=newuserinfo)
    newuser.grid(row=8,column=1,sticky='w',padx=10)

    Label(regisframe,text="Password  : ",bg='#8ac4d0',fg='#f6f5f5').grid(row=9,column=0,sticky='e',padx=10) 
    newpwd = Entry(regisframe,width=20,bg='#a1cae2',textvariable=newpwdinfo,show='*')
    newpwd.grid(row=9,column=1,sticky='w',padx=10)

    Label(regisframe,text="Confirm Password  : ",bg='#8ac4d0',fg='#f6f5f5').grid(row=10,column=0,sticky='e',padx=10) 
    cfpwd = Entry(regisframe,width=20,bg='#a1cae2',textvariable=cfinfo,show='*')
    cfpwd.grid(row=10,column=1,sticky='w',padx=10)

    regisaction = Button(regisframe,text="Register Submit",command=registration,bg='blue')
    regisaction.grid(row=11,column=1,columnspan=2,ipady=5,ipadx=5,pady=5)
    cancle = Button(regisframe,text="Cancel",command=cancles)
    cancle.grid(row=11,column=0,ipady=5,ipadx=5,pady=5)
    
    stid.focus_force()
    regisframe.grid(row=1,column=1,columnspan=2,rowspan=2,sticky='news')

def cancles():
                stid.delete(0,END)
                firstname.delete(0,END)
                lastname.delete(0,END)
                newuser.delete(0,END)
                newpwd.delete(0,END)
                cfpwd.delete(0,END)
                loginlayout(root) 
                       
def registration() :
    #print("Hello from registration")
    if stid.get() == "":
        messagebox.showwarning("Admin","Enter Studen ID")
        stid.focus_force()

    elif firstname.get() == "" :
        messagebox.showwarning("Admin","Please enter First Name")
        firstname.focus_force() 

    elif lastname.get() == "" :
        messagebox.showwarning("Admin","Please enter Last Name")
        lastname.focus_force()

    elif newuser.get() == "" :
        messagebox.showwarning("Admin","Please enter new Username")
        newuser.focus_force()

    elif newpwd.get() == "" :
        messagebox.showwarning("Admin","Please enter new Password")
        newpwd.focus_force()

    elif cfpwd.get() == "" :
        messagebox.showwarning("Admin","Please enter Comfirm Password ")
        cfpwd.focus_force()

    else :
        sql = "select * from student where USERNAME = ? "
        curr.execute(sql,[newuserinfo.get()])
        result = curr.fetchall()
        if result :
            messagebox.showwarning("Admin","Student ID is already exit \n Please try again")
            newuser.select_range(0,END)
            newuser.focus_force()
        else :
            if newuserinfo.get() == cfinfo.get():
                sql = "insert into student values(?,?,?,?,?,?,?)"
                curr.execute(sql,[ids.get(),fname.get(),lname.get(),newuserinfo.get(),newpwdinfo.get(),])
                conn.commit()
                messagebox.showinfo("Admin","Register Successfully")
                retrivedata()
                stid.delete(0,END)
                firstname.delete(0,END)
                lastname.delete(0,END)
                newuser.delete(0,END)
                newpwd.delete(0,END)
                cfpwd.delete(0,END)

            
            else :
                messagebox.showwarning("Admin",'The confirm password not match')
                cfpwd.select_range(0,END)
                cfpwd.focus_force()


def retrivedata() :
    sql = "select * from student"
    curr.execute(sql)
    result = curr.fetchall()
    print("Total row = ",len(result))
    for i,data in enumerate(result) :
        print("Row#",i+1,data)               
                   
createconnection()
root = mainwindow()
regisframe = Frame(root)

userinfo = StringVar()
pwdinfo = StringVar()


img1 = PhotoImage(file='p2.png').subsample(1)
loginlayout(root)

loginlayout(root)

ids = StringVar()
fname = StringVar()
lname = StringVar()
newuserinfo = StringVar()
newpwdinfo = StringVar()
cfinfo = StringVar()

roots.mainloop()
curr.close()
conn.close()
